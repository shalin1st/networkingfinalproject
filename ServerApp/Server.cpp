#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <thread>
#include <future>
#include "buffer.h"
#include <errno.h> 
#include <sys/types.h> 
#include <map> 
#include "ClientModel.h"
#include <ctime>
#include <cstdlib>
#include <random>
#include <algorithm>
#include <iterator>


#pragma comment (lib, "Ws2_32.lib")


#define TRUE   1 
#define FALSE  0 
#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "2752"

std::map<int, std::string> gMapLobbyToSessions;
std::map<int, int> gMapClientToRequests;
std::map<std::string, ClientModel*> gMapClientToSessions;
std::vector<ClientModel*> gVecClientSockets;
buffer sendBuffer(512);
buffer recvBuffer(512);

std::vector<std::string> explodeString(std::string inputString, char delim)
{
	std::vector<std::string> result;
	std::istringstream iss(inputString);

	if (inputString != "") {
		for (std::string token; std::getline(iss, token, delim); )
		{
			if (!token.empty() && token != "")
				result.push_back(std::move(token));
		}
	}

	return result;
}



bool sendMessageToClients(SOCKET clientSocket,std::string message, std::string room,int msgType)
{
	
	std::string sendString = "";
	if (msgType != 9) {
		int roomlen = room.length();
		int messageLength = message.length();
		int packetLength = 0;

		if (msgType == 0) {
			packetLength = roomlen  + 12;
		}
		else
			packetLength = roomlen +  messageLength + 16;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(msgType);
		sendBuffer.writeInt32BE(roomlen);
		sendBuffer.writeString(room);
		if (msgType > 0) {
			sendBuffer.writeInt32BE(messageLength);
			sendBuffer.writeString(message);
		}

		sendString = sendBuffer.readString(packetLength);
		sendBuffer.resetIndicesManually();
	}
	else
		sendString = message;

	int sendLength = sendString.length();
	int iResult = send(clientSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("Error in send ID: %d\n", WSAGetLastError());
		closesocket(clientSocket);
		WSACleanup();
		return 0;
	}
	

	return 1;
}

bool sendMessageToAuthServer(buffer recvBuffer,ClientModel* client,SOCKET clientSocket, std::string message, int msgType)
{
	int packetLength = 0;
	if (msgType == 0) {
		int msgID = msgType;
		int messageLength = message.length();	
		packetLength = messageLength + 16;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(msgID);
		sendBuffer.writeInt32BE(client->socketDescriptor);
		sendBuffer.writeInt32BE(messageLength);
		sendBuffer.writeString(message);
	}
	else if (msgType == 1) {
		int msgID = msgType;
		int emailLength = recvBuffer.readInt32BE();
		std::string email = recvBuffer.readString(emailLength);
		int passwordLength = recvBuffer.readInt32BE();
		std::string password = recvBuffer.readString(passwordLength);
		packetLength = emailLength + passwordLength + 20;
		
		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(msgID);
		sendBuffer.writeInt32BE(client->socketDescriptor);
		sendBuffer.writeInt32BE(emailLength);
		sendBuffer.writeString(email);
		sendBuffer.writeInt32BE(passwordLength);
		sendBuffer.writeString(password);
	}
	else if (msgType == 2) {
		int msgID = msgType;
		int emailLength = recvBuffer.readInt32BE();
		std::string email = recvBuffer.readString(emailLength);
		int passwordLength = recvBuffer.readInt32BE();
		std::string password = recvBuffer.readString(passwordLength);
		packetLength =  emailLength + passwordLength + 20;
		
		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(msgID);
		sendBuffer.writeInt32BE(client->socketDescriptor);
		sendBuffer.writeInt32BE(emailLength);
		sendBuffer.writeString(email);
		sendBuffer.writeInt32BE(passwordLength);
		sendBuffer.writeString(password);
	}
	else if (msgType == 3) {
		int msgID = msgType;
		packetLength =  12;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(msgID);
		sendBuffer.writeInt32BE(client->socketDescriptor);
	}
	else if (msgType == 4) {
		int msgID = msgType;
		packetLength = 12;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(msgID);
		sendBuffer.writeInt32BE(client->socketDescriptor);
	}
	else if (msgType == 5) {
		int msgID = msgType;
		int mapLength          = recvBuffer.readInt32BE();
		std::string map        = recvBuffer.readString(mapLength);
		int lobbyNameLength    = recvBuffer.readInt32BE();
		std::string lobbyname  = recvBuffer.readString(lobbyNameLength);
		int gameModeLength     = recvBuffer.readInt32BE();
		std::string gameMode   = recvBuffer.readString(gameModeLength);
		int Playersmaxlen   = recvBuffer.readInt32BE();
		std::string maxPlayers = recvBuffer.readString(Playersmaxlen);
		packetLength = mapLength + lobbyNameLength + gameModeLength + Playersmaxlen + 28;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(msgID);
		sendBuffer.writeInt32BE(client->socketDescriptor);
		sendBuffer.writeInt32BE(mapLength);
		sendBuffer.writeString(map);
		sendBuffer.writeInt32BE(lobbyNameLength);
		sendBuffer.writeString(lobbyname);
		sendBuffer.writeInt32BE(gameModeLength);
		sendBuffer.writeString(gameMode);
		sendBuffer.writeInt32BE(Playersmaxlen);
		sendBuffer.writeString(maxPlayers);
	}
	else if (msgType == 6) {
		int msgID = msgType;
		int lobbyLength = recvBuffer.readInt32BE();
		std::string lobby = recvBuffer.readString(lobbyLength);
		packetLength =  lobbyLength + 16;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(msgID);
		sendBuffer.writeInt32BE(client->socketDescriptor);
		sendBuffer.writeInt32BE(lobbyLength);
		sendBuffer.writeString(lobby);
		
	}
	else if (msgType == 7) {
		int msgID = msgType;
		int clientSocket = client->socketDescriptor;
		int lobbyLength = recvBuffer.readInt32BE();
		std::string lobby = recvBuffer.readString(lobbyLength);
		packetLength =  lobbyLength + 16;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(msgID);
		sendBuffer.writeInt32BE(clientSocket);
		sendBuffer.writeInt32BE(lobbyLength);
		sendBuffer.writeString(lobby);
	}

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();
	sendBuffer.resetIndicesManually();

	int iResult = send(clientSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("Error in send: %d\n", WSAGetLastError());
		closesocket(clientSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}


ClientModel* getClientModelbysocketId(int clientSocket) {
	for (int i = 0; i < ::gVecClientSockets.size(); i++) {
		if (::gVecClientSockets[i]->socketDescriptor == clientSocket)
			return ::gVecClientSockets[i];
		else
			return NULL;
	}
}

void processMessage(buffer recvBuffer,ClientModel* client,std::string inputStringData) {
	int packetLength = recvBuffer.readInt32BE();
	int msgID = recvBuffer.readInt32BE();
	if (msgID == 0) {
		int clientSocket = recvBuffer.readInt32BE();
		ClientModel* client = getClientModelbysocketId(clientSocket);
		sendMessageToClients(client->socketDescriptor,"","Welcome",0);
	}
	else if (msgID == 1)
	{
		int clientSocket = recvBuffer.readInt32BE();
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		sendMessageToClients(clientSocket, message, "Welcome", 1);
	}
	else if (msgID == 2)
	{
		sendMessageToAuthServer(recvBuffer,client,::gVecClientSockets[0]->socketDescriptor, "", 1);
	}
	else if (msgID == 3)
	{
		int clientSocket = recvBuffer.readInt32BE();
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		sendMessageToClients(clientSocket,  message, "Login", 2);
	}
	else if (msgID == 4)
	{
		sendMessageToAuthServer(recvBuffer, client,::gVecClientSockets[0]->socketDescriptor, "", 2);
	}
	else if (msgID == 5)
	{
		int clientSocket = recvBuffer.readInt32BE();
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		sendMessageToClients(clientSocket,  message, "Register", 3);
	}
	else if (msgID == 6)
	{
		sendMessageToAuthServer(recvBuffer, client, ::gVecClientSockets[0]->socketDescriptor, "", 3);
	}
	else if (msgID == 7)
	{
		int clientSocket = recvBuffer.readInt32BE();
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		sendMessageToClients(clientSocket,  message, "Logout", 4);

	}
	else if (msgID == 8)
	{
		sendMessageToAuthServer(recvBuffer, client, ::gVecClientSockets[0]->socketDescriptor, "", 4);
	}
	else if (msgID == 9)
	{
		int clientSocket = recvBuffer.readInt32BE();
		sendMessageToClients(clientSocket,  inputStringData, "Logout", 9);
	}
	else if (msgID == 10)
	{
		sendMessageToAuthServer(recvBuffer, client, ::gVecClientSockets[0]->socketDescriptor, inputStringData, 5);
	}
	else if (msgID == 11)
	{
		int clientSocket = recvBuffer.readInt32BE();
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		gMapLobbyToSessions[clientSocket] = message;
		sendMessageToClients(clientSocket,  message, "LobbyCreated", 5);
	}
	else if (msgID == 12)
	{
		sendMessageToAuthServer(recvBuffer, client, ::gVecClientSockets[0]->socketDescriptor, "", 6);
	}
	else if (msgID == 13)
	{
		int clientSocket = recvBuffer.readInt32BE();
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		sendMessageToClients(clientSocket, message, "LobbyJoined", 6);
			
	}
	else if (msgID == 14)
	{
		sendMessageToAuthServer(recvBuffer, client, ::gVecClientSockets[0]->socketDescriptor, "", 7);
	}
	else if (msgID == 15)
	{
		int clientSocket = recvBuffer.readInt32BE();
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		sendMessageToClients(clientSocket, message, "LobbyLeft", 7);

	}
	else if (msgID == 16)
	{
		int clientSocket = recvBuffer.readInt32BE();
		int sessionTokensKicked = recvBuffer.readInt32BE();
		std::string message = "Padding for Lobby Name ||You have Left the Lobby";
		::gMapLobbyToSessions.erase(clientSocket);
		sendMessageToClients(clientSocket,  message, "LobbyLeft", 7);
		if(sessionTokensKicked > 0 ){
			for (int i = 0; i < sessionTokensKicked; i++) {
				int brodcastClientSocket = recvBuffer.readInt32BE();
				::gMapLobbyToSessions.erase(brodcastClientSocket);
				message = "The host left the lobby, you have been forced out!";
				sendMessageToClients(brodcastClientSocket,  message, "BootedFromLobby", 8);
			}
		}
	}
	return;

}

void closeClientConnection(SOCKET clientSocket, fd_set &readfds) {
		int iResult = closesocket(clientSocket);
		if (iResult == SOCKET_ERROR)
			printf("Error in closing socket...\n");
		else
			printf("connection closed...\n");
		FD_CLR(clientSocket, &readfds);
		
		ClientModel * client = getClientModelbysocketId(clientSocket);

		if (::gMapLobbyToSessions.find(clientSocket) != ::gMapLobbyToSessions.end()) {
			recvBuffer.resetIndicesManually();
			std::string lobby = ::gMapLobbyToSessions[clientSocket];
			if(lobby != ""){
				recvBuffer.writeInt32BE(clientSocket);	
				recvBuffer.writeInt32BE(lobby.length());
				recvBuffer.writeString(lobby);
				sendMessageToAuthServer(recvBuffer,client, ::gVecClientSockets[0]->socketDescriptor, "", 7);
			}
		}

		recvBuffer.resetIndicesManually();
		recvBuffer.writeInt32BE(clientSocket);
		sendMessageToAuthServer(recvBuffer,client ,::gVecClientSockets[0]->socketDescriptor, "", 3);

		printf("Removed Socket from FD SET...\n");
		for (int index = 0; index < ::gVecClientSockets.size(); index++) {
			if (::gVecClientSockets[index]->socketDescriptor == clientSocket) {
				::gVecClientSockets.erase(::gVecClientSockets.begin() + index);
				break;
			}
		}
}

int main(int argc, char *argv[])
{
	int opt = TRUE;
	int acti,sd;
	int sd_max;

	WSADATA wsaData;
	int iResult;

	SOCKET masterSocket = INVALID_SOCKET;
	SOCKET clientSocket = INVALID_SOCKET;


	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	fd_set readfds;

	
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}


	masterSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (masterSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	if (setsockopt(masterSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,
		sizeof(opt)) < 0)
	{
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	
	iResult = bind(masterSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(masterSocket);
		WSACleanup();
		return 1;
	}

	

	iResult = listen(masterSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(masterSocket);
		WSACleanup();
		return 1;
	}

	
	puts("Waiting........");

	while (TRUE)
	{
		
		FD_ZERO(&readfds);

		
		FD_SET(masterSocket, &readfds);
		sd_max = masterSocket;


	
		for (int index = 0; index < gVecClientSockets.size(); index++)
		{
			
			sd = gVecClientSockets[index]->socketDescriptor;

			
			if (sd > 0)
				FD_SET(sd, &readfds);

			
			if (sd > sd_max)
				sd_max = sd;
		}

		
		acti = select(sd_max + 1, &readfds, NULL, NULL, NULL);

		if ((acti < 0) && (errno != EINTR))
		{
			printf("select error");
		}

		
		if (FD_ISSET(masterSocket, &readfds))
		{
			clientSocket = accept(masterSocket, NULL, NULL);
			if (clientSocket == INVALID_SOCKET) {
				printf("accept failed with error: %d\n", WSAGetLastError());
			}

			//add new socket to array of sockets 
			ClientModel* tmpClientModel = new ClientModel();
			tmpClientModel->socketDescriptor = clientSocket;
			std::time_t epochTime = std::time(0);
			std::stringstream ss;
			ss << epochTime;
			std::string strEpochTime = ss.str();
			tmpClientModel->ClientName = tmpClientModel->ClientName + strEpochTime;

			std::string message = "";
			gVecClientSockets.push_back(tmpClientModel);			
			clientSocket = INVALID_SOCKET;

			puts("Welcome/Hello sent successful");
		}


		int authServerSocket = gVecClientSockets[0]->socketDescriptor;

		for (int index = 0; index < gVecClientSockets.size(); index++)
		{
			sd = gVecClientSockets[index]->socketDescriptor;

			if (FD_ISSET(sd, &readfds))
			{
				iResult = recv(sd, recvbuf, recvbuflen, 0);
				if (iResult > 0) {
					printf("No of Bytes received: %d\n", iResult);
					std::string getData = "";
					recvBuffer.displayIndices();
					recvBuffer.resetIndicesManually(); //Hack as write Index gets restored .... need to understand
					for (int i = 0; i < 4; i++)
						getData += recvbuf[i];
					recvBuffer.writeString(getData);
					recvBuffer.displayIndices();
					int recvSize = recvBuffer.readInt32BE();
					recvBuffer.displayIndices();
					for (int i = 4; i < recvSize; i++)
						getData += recvbuf[i];
					recvBuffer.writeString(getData);
					recvBuffer.displayIndices();
					processMessage(recvBuffer, gVecClientSockets[index],getData);
				}
				else if (iResult == 0) {
					closeClientConnection(sd, readfds);
				}
				else {
					printf("recv failed with error: %d\n", WSAGetLastError());
					closeClientConnection(sd, readfds);
					//return 1;
				}
			}
		}
	}

	WSACleanup();
	return 0;
}
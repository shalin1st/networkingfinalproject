#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <conio.h>
#include <algorithm>
#include "buffer.h"
#include "Lobbydetails.h"
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512

bool terminate1 = false;


buffer sendBuffer(512);
buffer recvBuffer(512);
SOCKET ConnectSocket = INVALID_SOCKET;
char recvbuf[DEFAULT_BUFLEN];
int recvbuflen = DEFAULT_BUFLEN;
int iResult;
bool globIsConnected = false;
bool globIsLobbyJoined = false;
bool globIsLoggedIn = false;
std::string userIn = "";
char keyIn;
std::string gSessionToken = "";
bool gIsLoggedIn = false;
std::string gLobbyJoined = "";
Lobbydetails *lobbyDetails;


bool connectToServerIP(std::string input)
{
	using namespace std::literals;
	std::istringstream inputstream(input);
	std::string ipaddr, addrport;
	inputstream >> ipaddr; 
	inputstream >> ipaddr;
	inputstream >> addrport;

	WSADATA wsaData;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;

	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 0;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	iResult = getaddrinfo(ipaddr.c_str(), addrport.c_str(), &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 0;
	}

	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 0;
		}

	
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 0;
	}

	u_long iMode = 1;
	iResult = ioctlsocket(ConnectSocket, FIONBIO, &iMode);
	if (iResult != 0) {
		printf("Making socket non-blocking failed : %d\n", iResult);
		return 0;
	}

	globIsConnected = true;
	std::cout << "You are now connected to the game server \n";
	return 1;
}

std::vector<std::string> explodeString(std::string inputString, char delim)
{
	std::vector<std::string> result;
	std::istringstream iss(inputString);

	if (inputString != "") {
		for (std::string token; std::getline(iss, token, delim); )
		{
			if (!token.empty() && token != "")
				result.push_back(std::move(token));
		}
	}

	return result;
}

bool loginUser(std::string input)
{
	std::istringstream iss(input);
	std::string userid, pass;
	iss >> userid; 
	iss >> userid;
	iss >> pass;

	int msgID = 2;
	int emailLength = userid.length();
	int passwordLength = pass.length();
	int packetLength = emailLength + passwordLength  + 16;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(msgID);
	sendBuffer.writeInt32BE(emailLength);
	sendBuffer.writeString(userid);
	sendBuffer.writeInt32BE(passwordLength);
	sendBuffer.writeString(pass);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

bool signUpUser(std::string input)
{
	std::istringstream iss(input);
	std::string userid, pass;
	iss >> userid; 
	iss >> userid;
	iss >> pass;

	int msgID = 4;
	int emailLength = userid.length();
	int passwordLength = pass.length();
	int packetLength = emailLength + passwordLength  + 16;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(msgID);
	sendBuffer.writeInt32BE(emailLength);
	sendBuffer.writeString(userid);
	sendBuffer.writeInt32BE(passwordLength);
	sendBuffer.writeString(pass);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}

bool logoutUser(std::string input) {
	int msgID = 6;
	int packetLength =  8;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(msgID);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

bool getactiveLobbies(std::string input) {
	int msgID = 8;
	int packetLength = 8;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(msgID);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}

bool createLobby(std::string input) {
	std::istringstream iss(input);
	std::string map, lobbyname, gameMode, maxPlayers;
	iss >> map; 
	iss >> map;
	iss >> lobbyname;
	iss >> gameMode;
	iss >> maxPlayers;

	int msgID = 10;
	int mapLength = map.length();
	int lobbyNameLength = lobbyname.length();
	int gameModeLength = gameMode.length();
	int Playersmaxlen = maxPlayers.length();
	int packetLength =  mapLength + lobbyNameLength + gameModeLength + Playersmaxlen + 24;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(msgID);
	sendBuffer.writeInt32BE(mapLength);
	sendBuffer.writeString(map);
	sendBuffer.writeInt32BE(lobbyNameLength);
	sendBuffer.writeString(lobbyname);
	sendBuffer.writeInt32BE(gameModeLength);
	sendBuffer.writeString(gameMode);
	sendBuffer.writeInt32BE(Playersmaxlen);
	sendBuffer.writeString(maxPlayers);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

bool joinLobby(std::string input)
{
	input.erase(0, 5); 

	for (int i = 0; i < input.length(); i++)
		if (input[i] == ' ')
			input[i] = '_';

	int lobbyLength = input.length();
	int messageId = 12;
	int packetLength = lobbyLength + 12;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageId);
	sendBuffer.writeInt32BE(lobbyLength);
	sendBuffer.writeString(input);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}

bool leaveLobby(std::string input)
{
	input.erase(0, 6);

	for (int i = 0; i < input.length(); i++)
		if (input[i] == ' ')
			input[i] = '_';

	int lobbyLength = input.length();
	int messageId = 14;
	int packetLength = lobbyLength  + 12;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageId);
	sendBuffer.writeInt32BE(lobbyLength);
	sendBuffer.writeString(input);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}

bool parseInputCommand(std::string userMessage)
{

	std::vector<std::string> result;
	std::istringstream iss(userMessage);
	std::string s;
	iss >> s;
	if      (s == "connect") {
		if(!globIsConnected)
			connectToServerIP(userMessage);
		else
		std::cout << "You are connected already\n";
	}
	else if (s == "lobby") {
		if (::globIsLoggedIn && !::globIsLobbyJoined)
			getactiveLobbies(userMessage);
		else if (!globIsConnected)
			std::cout << "You need to connect to a game server first\n";
		else if (::globIsLoggedIn == false)
			std::cout << "You cannot view the lobby browser wothout logging in\n";
		else if (globIsLobbyJoined)
			std::cout << "Sorry, you need to leave the lobby to browse the games\n"; 
	}
	else if (s == "create") {
		if (globIsLoggedIn && !globIsLobbyJoined)
			createLobby(userMessage);
		else if (!globIsConnected)
			std::cout << "You need to be connected to  game server first! \n";
		else if (!globIsLoggedIn)
			std::cout << "You need to log in tocreate a lobby\n";
		else if (globIsLobbyJoined)
			std::cout << "Cannot create a lobby while inside this lobby\n";
	}
	else if (s == "join") {
		if (globIsLoggedIn && !globIsLobbyJoined)
			joinLobby(userMessage);
		else if (!globIsConnected)
			std::cout << "You need to connect to a game server first \n";
		else if (!globIsLoggedIn)
			std::cout << "Cannot join the lobby without loggin in\n";
		else if (globIsLobbyJoined)
			std::cout << "You are already here" + gLobbyJoined + ", You can leave this one to join another lobby\n";
	}
	else if (s == "leave") {
		if (globIsLoggedIn && globIsLobbyJoined)
			leaveLobby(userMessage);
		else if (!globIsConnected)
			std::cout << "Please connect to a game server\n";
		else if (!globIsLoggedIn)
			std::cout << "Cannot leave a lobby without joining in!\n";
		else if (!globIsLobbyJoined)
			std::cout << "You have to be in a lobby to leave\n";
	}
	else if (s == "signup")
	{
		if (!globIsLoggedIn && globIsConnected)
			signUpUser(userMessage);
		else if (!globIsConnected)
			std::cout << "PLease connect to a game server \n";
		else if (globIsLoggedIn)
			std::cout << "You are registered and logged in!!\n";
	}
	else if (s == "login")
	{
		if (!globIsLoggedIn && globIsConnected)
			loginUser(userMessage);
		else if (!globIsConnected)
			std::cout << "Connect to a game server \n";
		else if (globIsLoggedIn)
			std::cout << "Already logged in ! Logout first to login again.\n";
	}
	else if (s == "logout") {
		if (globIsLoggedIn)
			logoutUser(userMessage);
		else if (!globIsConnected)
			std::cout << "Connect to a game server first!!\n";
		else if (!globIsLoggedIn)
			std::cout << "You need to login in order to logout .\n";
	}
	else if (s == "close") {
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	else
	{
		std::cout << "\n\nThe command doesnt seem right, maybe try again? \n ";
		printf("\n\nTo connect, connect <ip> <port> \n to register, signup <id> <password> \n to login, login <id> <password> \n to join, join <lobbyname> \n to leave a lobby, leave <lobbyname>, \n create a lobby with, create <mapname> <lobbyname> <gamemode> ");
	}
	return 1;
}

void recvMessage()
{
	int packetLength = recvBuffer.readInt32BE();
	int msgID = recvBuffer.readInt32BE();
	if (msgID == 0)
	{
		int roomlen = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomlen);
		std::cout << "\n\nYou have been successfully connected to the game server. Please login or register to access the lobbies !\n";
	}
	else if (msgID == 1)
	{
		int roomlen = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomlen);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::cout << message << "\n";
	}
	else if (msgID == 2)
	{
		int roomlen = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomlen);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::cout << message << "\n";
		globIsLoggedIn = true;
		lobbyDetails = new Lobbydetails();
		lobbyDetails->getAllDetails();
		std::cout << "\n \n Game Lobby Browser \n";
		getactiveLobbies("");
	}
	else if (msgID == 3)
	{
		int roomlen = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomlen);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::cout << message << "\n";
	}
	else if (msgID == 4)
	{
		int roomlen = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomlen);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::cout << message << "\n";
		globIsLoggedIn = false;
	}
	else if (msgID == 5)
	{
		int roomlen = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomlen);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		globIsLobbyJoined = true;
		std::cout << "\n\nCreated and joined the lobby " + message + " successfully." << "\n";
	}
	else if (msgID == 6)
	{
		int roomlen = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomlen);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		globIsLobbyJoined = true;
		std::cout << message << "\n";
	}
	else if (msgID == 7)
	{
		int roomlen = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomlen);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		globIsLobbyJoined = false;
		std::cout << message << "\n";
	}
	else if (msgID == 8)
	{
		int roomlen = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomlen);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		::gLobbyJoined = "";
		std::cout << message << "\n";
		getactiveLobbies("");
	}
	else if (msgID == 9)
	{
		int discard = recvBuffer.readInt32BE();
		int rowCount = recvBuffer.readInt32BE();
		if (rowCount == 0) {
			std::cout << "No lobbies available \n";
		}
		else{
			for (int i = 0; i < rowCount; i++) {
				for (int j = 1; j < 10; j++) {
					int tmpLength = 0; std::string tmpString = "";
					tmpLength = recvBuffer.readInt32BE();
					tmpString = recvBuffer.readString(tmpLength);
					std::cout << tmpString << " || ";
				}
				std::cout << "\n";
			}
		}
	}
	return;
}


int __cdecl main(int argc, char **argv)
{
	std::cout << "================Welcome to my game lobby ! Please connnect to a game server first\n";
	std::cout << "Available server is : 127.0.0.1 . Type connect ip port to connect\n";
	bool isMessagePending = false;
	int recvSize = 0;
	int pendingMessageSize = 0;
	std::string partialMessage = "";
	
		do 
		{
			if (_kbhit()) 
			{
				char keyIn;
				keyIn = _getch();
				if (keyIn == '\r')
				{
					std::cout << '\r';
					for (int i = 0; i < userIn.length(); i++)
						std::cout << ' ';
					std::cout << '\r';
					terminate1 = !parseInputCommand(userIn);
					userIn = "";
				}
				else if (keyIn == 127 || keyIn == 8) 
				{
					userIn = userIn.substr(0, userIn.length() - 1);
					std::cout << keyIn << ' ' << keyIn;
				}
				else
				{
					userIn += keyIn;
					std::cout << keyIn;
				}
			}

			iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
			if (iResult > 0)
			{
				std::string getData = "";
				for (int i = 0; i < 4; i++)
					getData += recvbuf[i];
				recvBuffer.writeString(getData);
				int recvSize = recvBuffer.readInt32BE();
				for (int i = 4; i < recvSize; i++)
					getData += recvbuf[i];
				recvBuffer.writeString(getData);

				recvMessage();

			}
			else if (iResult == 0)
				
				continue;
		} while (!terminate1);

		
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
}
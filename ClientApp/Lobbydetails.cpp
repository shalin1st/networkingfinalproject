#include "Lobbydetails.h"



Lobbydetails::Lobbydetails()
{
	this->maps = new std::string[5]{ "DesertStorm","LosSantos", "SanAndreas", "Venice", "London" };
	this->gameModes = new std::string[5]{"Deathmatch","Defence","PlantTheBomb","Berserk","KillEmAll"};
}


Lobbydetails::~Lobbydetails()
{
}

void Lobbydetails::getAllDetails() {
	std::cout << "Use the map and gamemode(Down below) to create the lobby:\n";
	this->getMaps();
	this->getGameModes();
}
void Lobbydetails::getMaps() {
	std::cout << "\n\nMaps: \n";
	for (int i = 0; i < 5; i++) {
		std::cout << this->maps[i] << "\n";
	}
}
void Lobbydetails::getGameModes() {
	std::cout << "\n\nGame Modes: \n";
	for (int i = 0; i < 5; i++) {
		std::cout << this->gameModes[i] << "\n";
	}
}

bool Lobbydetails::checkLobbyIsValid(std::string map, std::string gameMode) {
	bool isMapValid = false;
	bool isGameModeValid = false;
	if (isPresentInArray(this->maps, map) == true)
		isMapValid = true;
	if (isPresentInArray(this->maps, map) == true)
		isGameModeValid = true;

	if (isMapValid && isGameModeValid)
		return true;
	if (isMapValid == false) {
		std::cout << "Map doesnt exist, Please use an existant one :( \n ";
		this->getMaps();
	}
	if (isGameModeValid == false) {
		std::cout << "Game Mode doesnt exist, Please use an existant one :( \n ";
		this->getGameModes();
	}
	return false;
}

bool Lobbydetails::isPresentInArray(std::string *strArray, std::string item) {
	for (int i = 0; i < strArray->size(); i++) {
		if (isStringEquals(strArray[i], item))
			return true;	
	}
	return false;
};

bool Lobbydetails::isStringEquals(std::string a, std::string b)
{
	unsigned int sz = a.size();
	if (b.size() != sz)
		return false;
	for (unsigned int i = 0; i < sz; ++i)
		if (tolower(a[i]) != tolower(b[i]))
			return false;
	return true;
}

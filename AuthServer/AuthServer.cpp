#define WIN32_LEAN_AND_MEAN

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <conio.h>
#include <algorithm>
#include <vector>
#include "buffer.h"
#include "DatabaseHelper.h"
#include "sha256.h"


// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "2752"

//Buffers we'll be storing data in
buffer sendBuffer(512);
buffer recvBuffer(512);

SOCKET ConnectSocket = INVALID_SOCKET;
int iResult;

static const char alphanumeric[] =
"0123456789"
"!@#$%^&*"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

int stringLength = sizeof(alphanumeric) - 1;

std::string gUserId = "";
std::string gUserName = "";
int welcomeMessage = 0;

std::vector<std::string> gVecUsersToKick;
std::vector<std::string> gVecUsersToBroadcast;


sql::ResultSet* gAllLobbies;

DatabaseHelper dbHelper;

std::string genSalt()
{
	std::string salty;

	for (int i = 0; i < 64; i++)
	{
		//int rando = rand() % 256;	Apparently, SQL doesn't like weird characters...
		//char c = char(rando);
		char c = alphanumeric[rand() % stringLength];
		salty += c;
	}

	return salty;
}

std::string hashPassword(std::string salt, std::string pass)
{
	std::string toHash = salt + pass;
	std::string output1 = sha256(toHash);
	return output1;
}

void resizeBufferIfRequired(buffer &bufferType, int packetLength)
{
	if (packetLength > DEFAULT_BUFLEN) {
		bufferType.resizeBuffer((unsigned int)packetLength);
	}
}

int createNewUser(std::string userId, std::string password)
{	
	if (password == "") //Some examples of invalid passwords, null and any string with spaces in it
		return 2;

	for (int i = 0; i < password.length(); i++)
	{
		if (password[i] == ' ')
			return 2;
	}

	//Check if the given email is already in the database
	std::string Queryvar = "SELECT * FROM lobbydb.userdetails WHERE username = '" + userId + "';";
	sql::ResultSet* result = dbHelper.ExecuteQuery(Queryvar);

	if (result->rowsCount() > 0) //This email address was found in the database
	{
		return 1;
	}

	//Generate a salt and hash
	std::string salty = genSalt();

	std::cout << "\n";
	std::string hashy = hashPassword(salty, password);

	
	//Finally, put all the data we've generated into the web_auth database
	std::string toExecute = "INSERT INTO lobbydb.userdetails(username, salt, password,createdOn) VALUES ('" + userId + "', '" + salty + "', '" + hashy + "',now());";
	//std::cout << toExecute << std::endl;
	int numUpdates = dbHelper.ExecuteUpdate(toExecute);

	if (numUpdates == 0)
	{
		return 3;
	}
	return 0;
}

int authenticateUser(std::string email, std::string password,int clientSocket)
{
	std::string Queryvar = "SELECT * FROM lobbydb.userdetails WHERE username = '" + email + "';";
	sql::ResultSet* result = dbHelper.ExecuteQuery(Queryvar);

	std::string userSalt, userHash, userID;
	std::string isLoggedIn = "-1";


	if (result->rowsCount() == 0) //This email doesn't exist in the database
	{
		return 1;
	}

	else if (result->rowsCount() > 1) //The email address exists more than once, so something is wrong.
	{
		return 2;
	}

	while (result->next())
	{
		userSalt = result->getString(3);
		userHash = result->getString(4);
		userID = result->getString(1);
		isLoggedIn = result->getString(5);
	}

	if (isLoggedIn == "1")
		return 3;

	std::string testHash = hashPassword(userSalt, password);

	if (testHash == userHash)
	{
		Queryvar = "UPDATE lobbydb.userdetails SET lastLogin = now(),isLoggedIn = '1',clientSocket = '"+std::to_string(clientSocket)+"' WHERE id = " + userID + ';';
		int numUpdates = dbHelper.ExecuteUpdate(Queryvar);
		return 0;
	}

	else
		return 1;

}


int removeSession(int clientSocket)
{
	//Check if the given email is already in the database
	std::string Queryvar = "SELECT * FROM lobbydb.userdetails WHERE clientSocket = '" + std::to_string(clientSocket) + "';";
	sql::ResultSet* result = dbHelper.ExecuteQuery(Queryvar);
	std::string userID;

	if (result->rowsCount() == 0) //This email doesn't exist in the database
	{
		return 1;
	}

	else if (result->rowsCount() > 1) //The email address exists more than once, so something is wrong.
	{
		return 2;
	}

	while (result->next())
	{
		userID = result->getString(1);
	}

	Queryvar = "UPDATE lobbydb.userdetails SET clientSocket = '0',isLoggedIn = '0' WHERE id = '" + userID + "';";
	int numUpdates = dbHelper.ExecuteUpdate(Queryvar);

	//Somehow the query returned no results. Probably a connection issue
	if (numUpdates == 0) {
		return 1;
	}
	return 0;
}

sql::ResultSet* getAllLobbies() {
	std::string Queryvar = "SELECT lm.id,lm.mapName,lm.lobbyname,lm.gameMode,lm.maxPlayers,lm.currentPlayers,acc.username,lm.isActive,lm.createdOn FROM lobbydb.gamelobbies lm JOIN lobbydb.userdetails acc ON lm.userId = acc.id where lm.isActive = '1' order by  lm.id desc;";
	sql::ResultSet* result = dbHelper.ExecuteQuery(Queryvar);
	return result;
}

int createNewLobby(int clientSocket, std::string map, std::string lobbyname, std::string gameMode, std::string maxPlayers) {
	std::string Queryvar = "SELECT * FROM lobbydb.userdetails WHERE clientSocket = '" + std::to_string(clientSocket) + "';";
	sql::ResultSet* result = dbHelper.ExecuteQuery(Queryvar);

	if (result->rowsCount() > 1) //This email address was found in the database
	{
		return 2;
	}

	std::string userId = "0";
	while (result->next()) {
		userId = result->getString(1);
	}
	
	if (userId == "0")
		return 1;

	Queryvar = "SELECT * FROM lobbydb.gamelobbies WHERE lobbyname = '" + lobbyname + "' ;";
	result = dbHelper.ExecuteQuery(Queryvar);

	if (result->rowsCount() > 0) //This email address was found in the database
	{
		return 3;
	}

	int numUpdates = dbHelper.ExecuteUpdate("INSERT INTO lobbydb.gamelobbies(mapName,lobbyname,gameMode,maxPlayers,currentPlayers,userId,isActive,createdOn) VALUES ('" + map + "','" + lobbyname + "','" + gameMode + "','" + maxPlayers + "', '1' ,'" + userId + "','1', now());");
	
	Queryvar = "SELECT * FROM lobbydb.gamelobbies WHERE lobbyname = '" + lobbyname + "' and userId = '"+ userId +"';";
	result = dbHelper.ExecuteQuery(Queryvar);

	if (result->rowsCount() > 1) //This email address was found in the database
	{
		return 2;
	}

	std::string lobbyId = "0";
	while (result->next()) {
		lobbyId = result->getString(1);
	}

	numUpdates = dbHelper.ExecuteUpdate("INSERT INTO lobbydb.playersinlobby(lobbyId,userId,isPresent,createdOn) VALUES ('" + lobbyId + "','" + userId + "','1', now());");

	//Somehow the query returned no results. Probably a connection issue
	if (numUpdates == 0) {
		numUpdates = dbHelper.ExecuteUpdate("Delete FROM lobbydb.gamelobbies where lobbyname = '" + lobbyname + "' and userId  = '" + userId + "';");
		return 1;
	}
	return 0;
}

int joinLobby (int clientSocket, std::string lobby) {
	std::string Queryvar = "SELECT * FROM lobbydb.gamelobbies WHERE lobbyname = '" + lobby + "';";
	sql::ResultSet* result = dbHelper.ExecuteQuery(Queryvar);

	if (result->rowsCount() > 1) //This email address was found in the database
	{
		return 2;
	}

	std::string lobbyId = "0";
	std::string lobbyHost = "0";
	std::string isActive = "-1";
	std::string currentPlayers = "-1";
	std::string maxPlayers = "-1";
	while (result->next()) {
		lobbyId = result->getString(1);
		maxPlayers = result->getString(5);
		currentPlayers = result->getString(6);
		lobbyHost = result->getString(7);
		isActive = result->getString(8);
	}

	if (lobbyId == "0")
		return 4;

	if (std::stoi(maxPlayers) - std::stoi(currentPlayers) <= 0)
		return 6;

	Queryvar = "SELECT * FROM lobbydb.userdetails WHERE clientSocket = '" + std::to_string(clientSocket)+ "';";
	result = dbHelper.ExecuteQuery(Queryvar);

	if (result->rowsCount() > 1 ) //This email address was found in the database
	{
		return 2;
	}

	std::string userId = "0";
	while (result->next()) {
		userId = result->getString(1);
		::gUserName = result->getString(2);
	}

	if (userId == "0")
		return 1;
	
	   
	if (userId != lobbyHost & isActive == "0")
		return 5;

	Queryvar = "SELECT * FROM lobbydb.playersinlobby WHERE lobbyId = '" + lobbyId + "' AND userId = '"+userId +"';";
	result = dbHelper.ExecuteQuery(Queryvar);

	if (result->rowsCount() > 1) //This email address was found in the database
	{
		return 2;
	}

	std::string lobPlayId = "0";
	std::string isPresent = "-1";
	while (result->next()) {
		lobPlayId = result->getString(1);
		isPresent = result->getString(4);
	}


	if (lobPlayId == "0" && isPresent == "-1") {
		int numUpdates = dbHelper.ExecuteUpdate("INSERT INTO lobbydb.playersinlobby(lobbyId,userId,isPresent,createdOn) VALUES ('" + lobbyId + "','" + userId + "','1', now());");
	}
	else if (lobPlayId != "0" && isPresent == "0") {
		Queryvar = "UPDATE lobbydb.playersinlobby SET isPresent = '1' WHERE id = '" + lobPlayId + "';";
		int numUpdates = dbHelper.ExecuteUpdate(Queryvar);
	}
	else if (lobPlayId != "0" && isPresent == "1") {
		return 3;
	}
	else
		return 1;

	int newCurrentPlayers = std::stoi(currentPlayers) + 1;

	Queryvar = "UPDATE lobbydb.gamelobbies SET isActive = '1',currentPlayers = '"+std::to_string(newCurrentPlayers)+"' WHERE id = '" + lobbyId + "';";
	int numUpdates = dbHelper.ExecuteUpdate(Queryvar);

	return 0;
}

int leaveLobby(int clientToken, std::string lobby) {
	std::string Queryvar = "SELECT * FROM lobbydb.gamelobbies WHERE lobbyname = '" + lobby + "';";
	sql::ResultSet* result = dbHelper.ExecuteQuery(Queryvar);

	if (result->rowsCount() > 1) //This email address was found in the database
	{
		return 2;
	}

	std::string lobbyId = "0";
	std::string hostId = "0";
	std::string currentPlayers = "-1";
	while (result->next()) {
		lobbyId = result->getString(1);
		currentPlayers = result->getString(6);
		hostId = result->getString(7);
	}

	if (lobbyId == "0")
		return 1;

	Queryvar = "SELECT * FROM lobbydb.userdetails WHERE clientSocket = '" + std::to_string(clientToken) + "';";
	result = dbHelper.ExecuteQuery(Queryvar);

	if (result->rowsCount() > 1) //This email address was found in the database
	{
		return 2;
	}

	std::string userId = "0";
	while (result->next()) {
		userId = result->getString(1);
		::gUserName = result->getString(2);
	}

	if (userId == "0")
		return 1;



	Queryvar = "SELECT * FROM lobbydb.playersinlobby WHERE lobbyId = '" + lobbyId + "' AND userId = '" + userId + "' and isPresent = '1' ;";
	result = dbHelper.ExecuteQuery(Queryvar);

	if (result->rowsCount() > 1) //This email address was found in the database
	{
		return 2;
	}

	std::string lobPlayId = "0";
	while (result->next()) {
		lobPlayId = result->getString(1);
	}

	if (lobPlayId == "0"){
		return 1;
	}

	Queryvar = "UPDATE lobbydb.playersinlobby SET isPresent = '0' WHERE id = '" + lobPlayId + "';";
	int numUpdates = dbHelper.ExecuteUpdate(Queryvar);

	int newCurrentPlayers = std::stoi(currentPlayers) - 1;
	if (newCurrentPlayers == 0)
		Queryvar = "UPDATE lobbydb.gamelobbies SET currentPlayers = '"+std::to_string(newCurrentPlayers)+"',isActive = '0' WHERE id = '" + lobbyId + "';";
	else
		Queryvar = "UPDATE lobbydb.gamelobbies SET currentPlayers = '" + std::to_string(newCurrentPlayers) + "' WHERE id = '" + lobbyId + "';";
	
	numUpdates = dbHelper.ExecuteUpdate(Queryvar);
	if (numUpdates == 0) {
		return 1;
	}

	return 0;
}

std::string getAllSessionTokensByUserId(std::vector<std::string> vectorOfIds)
{
	std::string sessionTokens = "";
	for (int count = 0; count < vectorOfIds.size(); count++) {
		std::string Queryvar = "SELECT * FROM lobbydb.userdetails WHERE id = '" + vectorOfIds[count] + "';";
		sql::ResultSet* result = dbHelper.ExecuteQuery(Queryvar);
		while (result->next()) {
			sessionTokens += result->getString(5) + "|";
		}
	}
	vectorOfIds.clear();
	if(sessionTokens != "")
		sessionTokens = sessionTokens.substr(0, sessionTokens.size() - 1);
	return sessionTokens;
}

std::vector<std::string> explodeString(std::string inputString, char delim)
{
	std::vector<std::string> result;
	std::istringstream iss(inputString);

	if (inputString != ""){
		for (std::string token; std::getline(iss, token, delim); )
		{
			if (!token.empty() && token != "")
				result.push_back(std::move(token));
		}
	}

	return result;
}

std::string vecToString(std::vector<std::string> vecToConvert)
{
	
	std::string result = "";

	for (int i = 0; i < vecToConvert.size(); i++) {
		result += vecToConvert[i] + "||";
	}

	return result;
}

bool sendMessage(int clientSocket,std::string message,int msgType)
{
	int messageId = -1;
	int packetLength = 0;
	int messageLength = 0;
	int rowCount = 0;
	int resultSize = 0;
	int tmpLength = 0;
	int lobbyLength = 0;
	std::string tmpString = "";
	std::vector<std::string> sessionTokensKicked;
	std::vector<std::string> sessionTokensToBeBroadcasted;
	std::vector<std::string> msgData;
	switch (msgType)
	{
	case 0:
		messageId = msgType;
		packetLength =  + 12;		

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageId);
		sendBuffer.writeInt32BE(clientSocket);
		break;
	case 1:
		messageId = msgType;
		
		messageLength = message.length();
		packetLength = messageLength + 16;


		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageId);
		sendBuffer.writeInt32BE(clientSocket);
		sendBuffer.writeInt32BE(messageLength);
		sendBuffer.writeString(message);
		break;

	case 2:
		messageId = 3;

		messageLength = message.length();
		packetLength =  messageLength + 16;

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageId);
		sendBuffer.writeInt32BE(clientSocket);
		sendBuffer.writeInt32BE(messageLength);
		sendBuffer.writeString(message);
		break;

	case 3:
		messageId = 5;

		messageLength = message.length();
		packetLength =  messageLength + 16;

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageId);
		sendBuffer.writeInt32BE(clientSocket);
		sendBuffer.writeInt32BE(messageLength);
		sendBuffer.writeString(message);
		break;

	case 4:
		messageId = 7;

		messageLength = message.length();
		packetLength =  messageLength + 16;

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageId);
		sendBuffer.writeInt32BE(clientSocket);
		sendBuffer.writeInt32BE(messageLength);
		sendBuffer.writeString(message);
		break;

	case 5:
		messageId = 9;
		rowCount = ::gAllLobbies->rowsCount();
		resultSize = 0;
		
		while (::gAllLobbies->next())
		{
			for (int i = 1; i < 10; i++) {
				resultSize += ::gAllLobbies->getString(i).length();
			}
		}

		packetLength =  resultSize + 16 + (rowCount*9*4);

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageId);
		sendBuffer.writeInt32BE(clientSocket);
		sendBuffer.writeInt32BE(rowCount);
		::gAllLobbies->beforeFirst();
		while (::gAllLobbies->next())
		{
			for (int i = 1; i < 10; i++) {
				tmpLength = 0; tmpString = "";
				tmpLength = ::gAllLobbies->getString(i).length();
				tmpString = ::gAllLobbies->getString(i);
				sendBuffer.writeInt32BE(tmpLength);
				sendBuffer.writeString(tmpString);
			}
		}
		break;

	case 6:
		messageId = 11;

		messageLength = message.length();
		packetLength =  messageLength + 16;


		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageId);
		sendBuffer.writeInt32BE(clientSocket);
		sendBuffer.writeInt32BE(messageLength);
		sendBuffer.writeString(message);
		break;
	
	case 7:
		messageId = 13;		

		messageLength = message.length();
		packetLength =  messageLength  +  16 ;


		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageId);
		sendBuffer.writeInt32BE(clientSocket);
		sendBuffer.writeInt32BE(messageLength);
		sendBuffer.writeString(message);
		break;

	case 8:
		messageId = 15;

		messageLength = message.length();
		packetLength =  messageLength + 16 ;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageId);
		sendBuffer.writeInt32BE(clientSocket);
		sendBuffer.writeInt32BE(messageLength);
		sendBuffer.writeString(message);
		break;

	case 9:
		messageId = 16;

		sessionTokensKicked = explodeString(message, '|');
			
		packetLength =  16 + (sessionTokensKicked.size() * 4) + (sessionTokensKicked.size() * 10);

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageId);
		sendBuffer.writeInt32BE(clientSocket);
		sendBuffer.writeInt32BE(sessionTokensKicked.size());
		if(sessionTokensKicked.size() > 0){
			for (int i = 0; i < sessionTokensKicked.size(); i++){
			sendBuffer.writeInt32BE(sessionTokensKicked[i].length());
			sendBuffer.writeString(sessionTokensKicked[i]);
			}		
		}
		break;
	
	default:
		messageId = -1;
		messageLength = 0;
		/*int roomlen = room.length();
		int messageLength = message.length();
		int msgID = 3;
		int packetLength = roomlen + messageLength + 16;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(msgID);
		sendBuffer.writeInt32BE(roomlen);
		sendBuffer.writeString(room);
		sendBuffer.writeInt32BE(messageLength);
		sendBuffer.writeString(message);

		std::string sendString = sendBuffer.readString(packetLength);
		int sendLength = sendString.length();*/
		break;
	}
	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendString.length(), 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

void processMessage(buffer recvBuffer) {
	int packetLength = recvBuffer.readInt32BE();
	int msgID = recvBuffer.readInt32BE();
	if (msgID == 0)
	{
	}
	else if (msgID == 1)
	{
		int clientSocket = recvBuffer.readInt32BE();
		int emailLength = recvBuffer.readInt32BE();
		std::string email = recvBuffer.readString(emailLength);
		int passwordLength = recvBuffer.readInt32BE();
		std::string password = recvBuffer.readString(passwordLength);
		int authResult = authenticateUser(email, password,clientSocket);
		if (authResult == 0)
		{
			std::string message = "Authenticated Successfully";
			std::cout << "Authenticated successfully." << std::endl;
			sendMessage(clientSocket,message, 2);

		}

		else if (authResult == 1)
		{
			std::string message = "--USERNAME or PASSWORD incorrect -----";
			std::cout << "Wrong Username or password." << std::endl;
			sendMessage(clientSocket,message, 1);

		}

		else if (authResult == 2)
		{
			std::string message = "Oops! Server error";
			std::cout << "Internal Server Error" << std::endl;
			sendMessage(clientSocket,message, 1);

		}
		else if (authResult == 2)
		{
			std::string message = "Oops! Server error";
			std::cout << "Internal Server Error" << std::endl;
			sendMessage(clientSocket, message, 1);

		}
		else if (authResult == 3)
		{
			std::string message = "The same user is already logged in to another window ";
			std::cout << "Another window active" << std::endl;
			sendMessage(clientSocket, message, 1);

		}
	}
	else if (msgID == 2)
	{
		int clientSocket = recvBuffer.readInt32BE();
		int emailLength = recvBuffer.readInt32BE();
		std::string email = recvBuffer.readString(emailLength);
		int passwordLength = recvBuffer.readInt32BE();
		std::string password = recvBuffer.readString(passwordLength);
		int creationResult = createNewUser(email, password);
		if (creationResult == 0)
		{
			std::string message = "Account created";
			std::cout << "Account created" << std::endl;
			sendMessage(clientSocket, message, 3);

		}

		else if (creationResult == 1)
		{
			std::string message = "Error while creating new account ! This email is already in use.";
			std::cout << "Error while creating new account ! This email is already in use." << std::endl;
			sendMessage(clientSocket, message, 1);

		}

		else if (creationResult == 2)
		{
			std::string message = "Error while creating new account! Invalid password.";
			std::cout << "Error while creating new account! Invalid password." << std::endl;
			sendMessage(clientSocket, message, 1);

		}
		else if (creationResult == 3)
		{
			std::string message = "Error while creating new account! Internal server error.";
			std::cout << "Error while creating new account! Internal server error." << std::endl;
			sendMessage(clientSocket, message, 1);

		}
	}
	else if (msgID == 3)
	{
		int clientSocket = recvBuffer.readInt32BE();
		int removalResult = removeSession(clientSocket);
		if (removalResult == 0)
		{
			std::string message = "Logged Out Successfully !";
			std::cout << "Logged Out Successfully !" << std::endl;
			sendMessage(clientSocket, message, 4);

		}

		else if (removalResult == 1)
		{
			std::string message = "Error while logging out. Session still active !";
			std::cout << "Error while logging out. Session still active !" << std::endl;
			sendMessage(clientSocket, message, 1);

		}

		else if (removalResult == 2)
		{
			std::string message = "Internal Server Error ! Please contact admin";
			std::cout << "Multiple sessions found. Please check." << std::endl;
			sendMessage(clientSocket, message, 1);

		}
	}
	else if (msgID == 4)
	{
		int clientSocket = recvBuffer.readInt32BE();
		::gAllLobbies = getAllLobbies();
		sendMessage(clientSocket,"",5);

	}
	else if (msgID == 5) {
		int clientSocket = recvBuffer.readInt32BE();
		int mapLength = recvBuffer.readInt32BE();
		std::string map = recvBuffer.readString(mapLength);
		int lobbyNameLength = recvBuffer.readInt32BE();
		std::string lobbyname = recvBuffer.readString(lobbyNameLength);
		int gameModeLength = recvBuffer.readInt32BE();
		std::string gameMode = recvBuffer.readString(gameModeLength);
		int Playersmaxlen = recvBuffer.readInt32BE();
		std::string maxPlayers = recvBuffer.readString(Playersmaxlen);
		int createResult = createNewLobby(clientSocket,map,lobbyname,gameMode,maxPlayers);
		if (createResult == 0)
		{
			//std::string message = "New lobby created successfully.";
			std::cout << "New lobby created successfully." << std::endl;
			sendMessage(clientSocket,lobbyname,6);
		}

		else if (createResult == 1)
		{
			std::string message = "Internal Server Error ! Please contact admin";
			std::cout << "Error while inserting new lobby." << std::endl;
			sendMessage(clientSocket, message, 1);
		}

		else if (createResult == 2)
		{
			std::string message = "Internal Server Error ! Please contact admin";
			std::cout << "Error while creating new lobby ! Multiple sessions found." << std::endl;
			sendMessage(clientSocket, message, 1);
		}
		else if (createResult == 3)
		{
			std::string message = "Lobby name already in use ! Please use another name";
			std::cout << "Lobby name already in use !" << std::endl;
			sendMessage(clientSocket, message, 1);
		}
	}
	else if (msgID == 6) {
		int clientSocket = recvBuffer.readInt32BE();
		int lobbyLength = recvBuffer.readInt32BE();
		std::string lobby = recvBuffer.readString(lobbyLength);
		int joinResult = joinLobby(clientSocket,lobby);
		if (joinResult == 0)
		{
			
			std::string message = "Joined lobby " + lobby + " successfully.";
			std::cout << "Joined lobby " + lobby + " successfully." << std::endl;
			sendMessage(clientSocket, message, 7);
		}

		else if (joinResult == 1)
		{
			std::string message = "Internal Server Error ! Please contact admin";
			std::cout << "Error while inserting into lobby players." << std::endl;
			sendMessage(clientSocket, message, 1);
		}

		else if (joinResult == 2)
		{
			std::string message = "Internal Server Error ! Please contact admin";
			std::cout << "Error while joining lobby ! Multiple lobbies found." << std::endl;
			sendMessage(clientSocket, message, 1);
		}
		else if (joinResult == 3)
		{
			std::string message = "Already joined the lobby"+lobby+". Please leave the lobby first to join another !";
			std::cout << "Already joined the lobby" + lobby + ". Please leave the lobby first to join another !" << std::endl;
			sendMessage(clientSocket, message, 1);
		}
		else if (joinResult == 4)
		{
			std::string message = "Lobby : " + lobby + " does not exist. Please check the available lobbies with command /viewlobby !";
			std::cout << "Lobby : " + lobby + " does not exist. Please check the available lobbies with command /viewlobby !" << std::endl;
			sendMessage(clientSocket, message, 1);
		}
		else if (joinResult == 5)
		{
			std::string message = "Lobby : " + lobby + " is not active. Please check the available lobbies with command /viewlobby !";
			std::cout << "Lobby : " + lobby + " is not active. Please check the available lobbies with command /viewlobby !" << std::endl;
			sendMessage(clientSocket, message, 1);
		}
		else if (joinResult == 6)
		{
			std::string message = "Cannot join the Lobby : " + lobby + " as it is currently full. Please check the other available lobbies with command /viewlobby !";
			std::cout << "Cannot join the Lobby : " + lobby + " as it is currently full. Please check the other available lobbies with command /viewlobby !" << std::endl;
			sendMessage(clientSocket, message, 1);
		}
	}
	else if (msgID == 7) {
		int clientSocket = recvBuffer.readInt32BE();
		int lobbyLength = recvBuffer.readInt32BE();
		std::string lobby = recvBuffer.readString(lobbyLength);
		int leaveResult = leaveLobby(clientSocket, lobby);
		if (leaveResult == 0)
		{
			std::string message = "Left lobby " + lobby + " successfully.";
			sendMessage(clientSocket, message, 8);
		}

		else if (leaveResult == 1)
		{
			std::string message = "Error while leaving lobby. You are not a part of this lobby";
			std::cout << "Error while leaving lobby players, player is not present in lobby" << std::endl;
			sendMessage(clientSocket, message, 1);
		}

		else if (leaveResult == 2)
		{
			std::string message = "Internal Server Error ! Please contact admin";
			std::cout << "Error while leaving lobby ! Multiple lobbies found." << std::endl;
			sendMessage(clientSocket, message, 1);
		}
		else if (leaveResult == 3)
		{
			std::string sessionTokensKicked = getAllSessionTokensByUserId(gVecUsersToKick);
			std::cout << "Host left the lobby "+ lobby +" ! Kicking other players as well "<< std::endl;
			sendMessage(clientSocket, sessionTokensKicked, 9);
		}
	}
	recvBuffer.resetIndicesManually();
}

int __cdecl main(int argc, char **argv)
{
	//CHANGE YOUR DETAILS HERE 
	if (!dbHelper.ConnectToDatabase("127.0.0.1:3306", "root", "", "lobbydb"))
	{
		std::cout << "Failed to connect to the database!" << std::endl;
		return 1;
	}

	using namespace std::literals;
	WSADATA wsaData;

	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	// Validate the parameters
	if (argc != 2) {
		printf("usage: %s server-name\n", argv[0]);
		return 1;
	}

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}

	while (true)
	{
		iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0)
		{
			std::string getData = "";
			recvBuffer.resetIndicesManually();
			for (int i = 0; i < 4; i++)
				getData += recvbuf[i];
			recvBuffer.writeString(getData);
			int recvSize = recvBuffer.readInt32BE();
			for (int i = 4; i < recvSize; i++)
				getData += recvbuf[i];
			recvBuffer.writeString(getData);
			processMessage(recvBuffer);

		}
		else if (iResult == 0)
			continue;
		else
			welcomeMessage++;
	}

	srand(time(NULL));

	return 0;
}